package Domain.Product;

import Domain.ProductBeheer.BusinessRuleType;
import Domain.ProductBeheer.CodeTemplate;
import Domain.ProductBeheer.ConnectionInfo;
import Domain.ProductBeheer.Operator;
import Domain.ProductBeheer.Other;
import Service.BusinessRuleService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "BUSINESSRULE")
public class BusinessRule implements Serializable {

    public static BusinessRule getBusinessRule(String id) {
        BusinessRuleService brs = new BusinessRuleService();
        return brs.getBusinessRule(id);
    }

    public static void saveCode(BusinessRule br, String s) {
        BusinessRuleService brs = new BusinessRuleService();
        brs.saveCode(br, s);
    }

    @Id
    @Column(name = "id")
    private String code;

    @Column(name = "description")
    private String description;

    @Column(name = "failuremessage")
    private String failureMessage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Operator")
    private Operator operator;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BUSINESSRULETYPE_ID")
    private BusinessRuleType type;
    //VALUE NOT WORKING YET
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "businessRule")
    private List<Value> values = new ArrayList<Value>();
    @Column(name = "CODE")
    private String gencode;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONNECTION_ID")
    private ConnectionInfo connection_id;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "businessRule")
    private List<Other> otheroperator = new ArrayList<Other>();

    public BusinessRule() {
    }

    public BusinessRule(String code) {
        this.code = code;
    }

    public String getGeneratedCode(String language) {
        System.out.println("running getGeneratedCode()");
        CodeTemplate ct = null;
        try{
            ct = type.getCodeTemplate(this, language);
        } catch(Exception e){
            System.out.println("CATCH CATCH CATCH CATCH CATCH");
            e.printStackTrace();
        }
        System.out.println("BusinessRule.getGeneratedCode(): Language: " + language);
        System.out.println("ALL PROPERTIES HAVE BEEN SET");
        if(ct != null){
            ct.setBusinessRule(this);
            ct.initializeTemplate();
            return ct.getCode(this);
        }
        return "BusinessRule.getGeneratedCode(): CodeTemplate = null";
    }
    
    public BusinessRuleType getBusinessRuleType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public List<Value> getValues() {
        return values;
    }

    public void setValues(List<Value> values) {
        this.values = values;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public List<Other> getOtheroperator() {
        return otheroperator;
    }

    public void setOtheroperator(List<Other> otheroperator) {
        this.otheroperator = otheroperator;
    }

    public String getFailure() {
        return failureMessage;
    }

    public String getGencode() {
        return gencode;
    }

    public void setGencode(String gencode) {
        this.gencode = gencode;
    }

    public ConnectionInfo getConnection_id() {
        return connection_id;
    }

    public void setConnection_id(ConnectionInfo connection_id) {
        this.connection_id = connection_id;
    }
}
