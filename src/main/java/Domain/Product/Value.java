package Domain.Product;

import Domain.ProductBeheer.Colomn;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Max
 */
@Entity
@Table(name = "VALUE")
public class Value implements Serializable {

    @Id
    private int value_id;
    @Column(name = "waarde")
    private String value;
    @Column(name = "type")
    private String type;
    @Column(name = "position")
    private int position;
    @OneToOne
    @JoinColumn(name = "kolomid")
    private Colomn cl;
    @ManyToOne(fetch = FetchType.LAZY)
    private BusinessRule businessRule;

    @Transient
    private String stringvalue;

    public Value() {
    }

    public Value(String value, int pos) {
        this.value = value;
        this.position = pos;
    }

    public Value(String colomn, String table) {
        cl = new Colomn();
        cl.setColomnname(colomn);
        cl.setTablename(table);
    }

    public Value(String ListAttributes) {
        stringvalue = ListAttributes;
    }

    public String getListValue() {
        return stringvalue;
    }

    public String getValue() {
        System.out.println("Value.getValue(): start");
        if (cl == null) {
            return value + "";
        } else {
            return cl.getColomnname();
        }
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTableName() {
        if (cl != null) {
            return cl.getTablename();
        } else {
            return "column name = null";
        }
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getType() {
        if(type == null){
            return "Type is null";
        }
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        if (cl != null) {
            return cl.getKey();
        } else {
            return "No id found for this value";
        }
    }

}
