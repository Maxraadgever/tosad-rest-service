package Domain.ProductBeheer;

import Domain.Product.BusinessRule;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class BusinessRuleType implements Serializable {

    @Id
    @Column(name = "BUSINESSRULETYPE_ID")
    private int id;
    private String name;
    private String description;
    @OneToOne
    @JoinColumn(name = "CATEGORY_CATEGORY_ID")
    private Category ct;
    @OneToMany(fetch = FetchType.LAZY, mappedBy="businessruletype")
    private List<CodeTemplate> temp = new ArrayList<CodeTemplate>();
    
    public BusinessRuleType() {
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public CodeTemplate getCodeTemplate(BusinessRule br, String language) {
        CodeTemplate template = null;
        for(CodeTemplate x : temp){
            x.setBusinessRule(br);
            if(x.getRuleType().equals(name) && x.getLanguage().equals(language)){
                System.out.println("FOUND ONE!!!!!!" + x.getRuleType() + " LANG " + x.getLanguage());
                template = x;
            }
        }
        if(template == null){
            System.out.println("FOUND NOTHING!!!!");
        }
        return template;
    }

    public void setTemp(List<CodeTemplate> temp) {
        this.temp = temp;
    }

    public Category getCt() {
        return ct;
    }

    public void setCt(Category ct) {
        this.ct = ct;
    }

}
