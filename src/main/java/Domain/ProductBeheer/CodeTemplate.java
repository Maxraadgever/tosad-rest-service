package Domain.ProductBeheer;

import Domain.Product.BusinessRule;
import Domain.Product.Value;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "CODETEMPLATE")
public class CodeTemplate implements Serializable {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "LANGUAGE")
    private String language;

    @Column(name = "TEMPLATE")
    private String template;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "businessruletype_id")
    private BusinessRuleType businessruletype;

    @Transient
    private BusinessRule br;
    @Transient
    private List<Value> values;
    @Transient
    private String value1, value2, value3;
    @Transient
    private String table;
    @Transient
    private String column2;
    @Transient
    private String tab2;

    public CodeTemplate() {
    }

    public CodeTemplate(BusinessRule br) {
        initializeTemplate();
    }

    public String getCode(BusinessRule br) {
        this.br = br;
        System.out.println("CodeTemplate.getCode(): start");
        String s = template;
        if (template == null) {
            System.out.println("Template is null wtf");
        }
        try {
            s = s.replace("<TABLE>", getTable())
                    .replace("<BRNAME>", "BR" + getBrName())
                    .replace("<VALUE1>", getValue1())
                    .replace("<OPERATOR>", getOperator())
                    .replace("<VALUE2>", getValue2())
                    .replace("<OTHER>", getOther())
                    .replace("<VALUE3>", getValue3())
                    .replace("<TABLE1>", getTable())
                    .replace("<TABLE2>", getTable2())
                    .replace("<COLUMN1>", getValue1())
                    .replace("<COLUMN2>", getValue2())
                    .replace("<TABLE1ID>", getTableId(1))
                    .replace("<TABLE2ID>", getTableId(2))
                    .replace("<FAILURE>", getFailure());
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("CodeTemplate.getCode(): result:" + s);
        return s;
    }

    public void setBusinessRule(BusinessRule br) {
        this.br = br;
    }

    public void initializeTemplate() {
        this.values = br.getValues();
        System.out.print("VALUES SIZE: " + values.size());
        for (Value v : values) {
            System.out.println("CodeTemplae.init(): " + v.getValue());
            if (v.getType().equals("Number List")) {
                String list = v.getValue();
                v.setValue(list.replaceAll(";", ","));
            }
            if (v.getPosition() == 1) {
                System.out.println("VALUE1 created: " + v.getValue());
                value1 = v.getValue();
                table = v.getTableName();
            } else if (v.getPosition() == 2) {
                System.out.println("VALUE2 created: " + v.getValue());
                value2 = v.getValue();
                tab2 = v.getTableName();
            } else if (v.getPosition() == 3) {
                System.out.println("VALUE3 created: " + v.getValue());
                value3 = v.getValue();
            } else {
                System.out.println("Value Error: One of the values[" + v.getValue() + "] does not have a position");
            }
        }
    }

    public String getOther() {
        System.out.println("getOther(): " + br.getOtheroperator().size());
        if (!br.getOtheroperator().isEmpty()) {
            if (br.getOtheroperator().get(0).getSource() != null) {
                return br.getOtheroperator().get(0).getSource();
            }else {
                return "other source is null";
            }
        } else {
            return "other is 0";
        }
    }

    public String getTable() {
        return table;
    }

    public String getRuleType() {
        return br.getBusinessRuleType().getName();
    }

    public String getCategory() {
        return br.getBusinessRuleType().getCt().getName();
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getBrName() {
        return br.getCode();
    }

    public String getValue1() {
        if (value1 != null) {
            return value1;
        } else {
            return "Value1 is null";
        }
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getValue2() {
        if (value2 != null) {
            return value2;
        } else {
            return "Value2 is null";
        }
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    public String getValue3() {
        if (value3 != null) {
            return value3;
        } else {
            return "Value3 is null";
        }
    }

    public void setValue3(String value3) {
        this.value3 = value3;
    }

    public String getColumn2() {
        return column2;
    }

    public void setColumn2(String column2) {
        this.column2 = column2;
    }

    public String getOperator() {
        return br.getOperator().toString();
    }

    public String getTable2() {
        if (tab2 != null) {
            return tab2;
        } else {
            return "tab2 is null";
        }
    }

    public void setTab2(String tab2) {
        this.tab2 = tab2;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTableId(int i) {
        String ret = "";
        for (Value v : values) {
            if (v.getPosition() == i) {
                ret = v.getId();
            }
        }
        if (ret == null) {
            ret = "CodeTemplate.getTableId: ID not found";
        }
        return ret;
    }

    public String getFailure() {
        if (br.getFailure() != null) {
            return br.getFailure();
        } else {
            return "Failure is null";
        }
    }
}
