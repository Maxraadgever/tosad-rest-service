package Domain.ProductBeheer;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Kolom")
public class Colomn implements Serializable {

    @Id
    @Column(name = "Kolomid")
    private int id;
    @Column(name = "KOLOMNAAM")
    private String colomnname;
    @Column(name = "TABLENAAM")
    private String tablename;
    @Column(name = "KEY")
    private String key;

    public void setColomnname(String name) {
        colomnname = name;
    }

    public void setTablename(String name) {
        tablename = name;
    }

    public String getColomnname() {
        return colomnname;
    }

    public String getTablename() {
        return tablename;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }
}
