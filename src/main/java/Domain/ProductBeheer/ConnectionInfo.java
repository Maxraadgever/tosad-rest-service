/*
 * Licsense Header
 */
package Domain.ProductBeheer;

import Domain.Product.BusinessRule;
import Service.ConnectionService;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Max
 */
@Entity
@Table(name = "CONNECTION")
public class ConnectionInfo implements Serializable {

    // STATIC METHODS //
    public static boolean testConnection(String id) {
        boolean b = false;
        ConnectionService cs = new ConnectionService();
        ConnectionInfo ci = cs.getConnection(id);
        try {
            System.out.println("Testing connection... : " + ci.getUrl());
            Connection conn = DriverManager.getConnection(ci.getUrl(), ci.getUsername(), ci.getPassword());
            b = conn.isValid(30);
            conn.close();
        } catch (Exception e) {
            System.out.println("No connection found");
            e.printStackTrace();
        }
        return b;
    }

    public static boolean createView(String id) {
        boolean b = false;
        ConnectionService cs = new ConnectionService();
        ConnectionInfo ci = cs.getConnection(id);
        try {
            System.out.println("Creating views...");
            Connection conn = DriverManager.getConnection(ci.getUrl(), ci.getUsername(), ci.getPassword());
            Statement stmt = conn.createStatement();
            stmt.executeQuery(ci.createTargetQuery());
            stmt.executeQuery("GRANT SELECT ON VIEW_TARGETDB to public");
            System.out.println("Ran target query");
            stmt.close();
            conn.close();
            conn = DriverManager.getConnection("jdbc:oracle:thin:@ondora02.hu.nl:8521:cursus02", "tosad_2015_2b_team4", "tosad_2015_2b_team4");
            stmt = conn.createStatement();
            stmt.executeQuery(ci.createToolQuery());

            System.out.println("Ran tool query");

            Statement stmt2 = conn.createStatement();
            String query = "UPDATE CONNECTION "
                    + "SET ACTIVE='0'"
                    + "WHERE ACTIVE='1'";
            stmt.executeQuery(query);
            String query2 = "UPDATE CONNECTION "
                    + "SET ACTIVE='1' "
                    + "WHERE CONNECTION_ID=" + ci.getConnection_id();
            System.out.println("THE QUERY IS: " + query2);
            stmt2.executeQuery(query2);
            stmt.close();
            stmt2.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }

    public static boolean execute(BusinessRule br, String connectionid) {
        boolean b = false;
        ConnectionService cs = new ConnectionService();
        ConnectionInfo ci = cs.getConnection(connectionid);
        try {
            Connection conn = DriverManager.getConnection(ci.getUrl(), ci.getUsername(), ci.getPassword());
            Statement stmt = conn.createStatement();
            try {
                if (br.getGencode().contains("ADD CONSTRAINT")) {
                    stmt.executeQuery("ALTER TABLE " + br.getValues().get(0).getTableName() + " DROP CONSTRAINT BR" + br.getCode());
                }
            } catch (Exception e) {
                System.out.println("No constraint found");
            }
            stmt.executeQuery(br.getGencode());
            stmt.close();
            conn.close();
            b = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }

    @Id
    private int connection_id;

    private String description;
    private String username;

    private String password;
    private String host;
    private String port;
    private String sid;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "connection_id")
    private List<BusinessRule> rules = new ArrayList<BusinessRule>();

    public ConnectionInfo() {
    }

    public String getUrl() {
        return "jdbc:oracle:thin:@" + getHost() + ":" + getPort() + ":" + getSid();
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getConnection_id() {
        return connection_id;
    }

    public void setConnection_id(int connection_id) {
        this.connection_id = connection_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public List<BusinessRule> getRules() {
        return rules;
    }

    public void setRules(List<BusinessRule> rules) {
        this.rules = rules;
    }

    private String createTargetQuery() {
        String s;
        s = "CREATE OR REPLACE VIEW " + username.toUpperCase() + ".VIEW_TARGETDB AS SELECT * FROM all_tab_columns WHERE owner = '" + username.toUpperCase() + "' AND NOT table_name = 'VIEW_TARGETDB'";
        System.out.println(s);
        return s;
    }

    private String createToolQuery() {
        String s;
        s = "CREATE OR REPLACE VIEW TOSAD_2015_2B_TEAM4.V_TARGETDB2 AS SELECT * FROM " + username + ".VIEW_TARGETDB";
        System.out.println(s);
        return s;
    }

}
