package Domain.ProductBeheer;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Operator implements Serializable {

    @Id
    private int operator_id;
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "operatorid")
    private Other other;
    private String type;

    public Operator() {
    }
    
    @Override
    public String toString() {
        String s = "";
        try {
            if (type.equals("greather than")) {
                s = " > ";
            } else if (type.equals("smaller than")) {
                s = " < ";
            } else if (type.equals("equal to")) {
                s = " = ";
            } else if (type.equals("not equal to")) {
                s = " != ";
            } else if (type.equals("greater than or equal to")) {
                s = " >= ";
            } else if (type.equals("smaller than or equal to")) {
                s = " <= ";
            } else if(type.equals("must be between")){
                s = " BETWEEN ";
            }else if (type.equals("may not be between")){
                s = " NOT BETWEEN ";
            }
            else {
                s = type;
            }
        } catch (NullPointerException ne) {
            System.out.println("Rule hieronder heeft geen operator nodig:");
        }
        return s;
    }

    public int getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(int operator_id) {
        this.operator_id = operator_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Other getOther() {
        return other;
    }

    public void setOther(Other other) {
        this.other = other;
    }

}
