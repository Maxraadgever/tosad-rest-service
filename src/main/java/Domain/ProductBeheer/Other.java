package Domain.ProductBeheer;

import Domain.Product.BusinessRule;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "OTHEROPERATOR")
public class Other implements Serializable {

    @Id
    private int other_id;
    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private Operator operatorid;
    @Column(name = "SOURCE")
    private String source;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BUSINESSRULE_ID")
    private BusinessRule businessRule;
    
    public Other() {
    }

    public int getOther_id() {
        return other_id;
    }

    public void setOther_id(int other_id) {
        this.other_id = other_id;
    }

    public String getSource() {
        System.out.println("SOURCE: "+ source);
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

}
