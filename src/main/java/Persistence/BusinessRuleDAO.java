package Persistence;

import Domain.Product.BusinessRule;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class BusinessRuleDAO extends GenericDAO<BusinessRule> {

    private EntityManagerFactory entityManagerFactory;

    public BusinessRuleDAO(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public BusinessRule getByID(String id) {
        System.out.println("GET BY ID " + id);
        BusinessRule br = null;
        try {
            EntityManager em = entityManagerFactory.createEntityManager();
            em.getTransaction().begin();
            br = em.find(BusinessRule.class, id);
            em.getTransaction().commit();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("GET BY ID");
        return br;
    }

    @Override
    public void create(BusinessRule instance) {
        System.out.println("CREATING 1");
        EntityManager em = entityManagerFactory.createEntityManager();
        System.out.println("CREATING 2");
        em.getTransaction().begin();
        System.out.println("CREATING 3");
        em.persist(instance);
        System.out.println("CREATING 4");
        em.getTransaction().commit();
        System.out.println("CREATING 5");
        em.close();
        System.out.println("CREATING 6");
    }

    @Override
    public void change(BusinessRule instance, BusinessRule newInstance) {
        EntityManager em = entityManagerFactory.createEntityManager();

        em.find(BusinessRule.class, instance.getCode());
        em.getTransaction().begin();
        instance.setGencode(newInstance.getGencode());
        em.merge(instance);
        em.getTransaction().commit();
        em.close();
        System.out.println("Code saved!");
    }

    @Override
    public void delete(BusinessRule instance) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<BusinessRule> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
