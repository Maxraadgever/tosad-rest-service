/*
 * Licsense Header
 */
package Persistence;

import Domain.ProductBeheer.CodeTemplate;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Max
 */
public class CodeTemplateDAO extends GenericDAO<CodeTemplate> {

    private EntityManagerFactory entityManagerFactory;

    public CodeTemplateDAO(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public CodeTemplate getByID(String id) {
        System.out.println("GET BY ID " + id);
        CodeTemplate ct = null;
        try {
            EntityManager em = entityManagerFactory.createEntityManager();

            ct = em.find(CodeTemplate.class, id);
            
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("CODETEMPLATE: GET BY ID");
        return ct;
    }

    @Override
    public void create(CodeTemplate instance) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void change(CodeTemplate instance, CodeTemplate newInstance) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(CodeTemplate instance) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<CodeTemplate> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
