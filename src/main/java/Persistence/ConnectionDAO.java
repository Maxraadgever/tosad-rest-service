/*
 * Licsense Header
 */
package Persistence;

import Domain.ProductBeheer.ConnectionInfo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Max
 */
public class ConnectionDAO extends GenericDAO<ConnectionInfo> {

    private EntityManagerFactory entityManagerFactory;

    public ConnectionDAO(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public ConnectionInfo getByID(String id) {
        System.out.println("GET BY ID " + id);
        ConnectionInfo conn = null;
        try {
            EntityManager em = entityManagerFactory.createEntityManager();
            conn = em.find(ConnectionInfo.class, Integer.parseInt(id));
            em.close();
            if(conn != null){
                System.out.println("ConnectionInfo is not null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("GET BY ID");
        return conn;
    }

    @Override
    public void create(ConnectionInfo instance) {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.persist(instance);
        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void change(ConnectionInfo instance, ConnectionInfo newInstance) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(ConnectionInfo instance) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ConnectionInfo> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
