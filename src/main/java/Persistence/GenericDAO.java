/*
 * Licsense Header
 */

package Persistence;

import java.util.List;

/**
 *
 * @author Max
 */
public abstract class GenericDAO<T> {

    public abstract T getByID(String id);
    public abstract void create(T instance);
    public abstract void change(T instance, T newInstance);
    public abstract void delete(T instance);
    public abstract List<T> getAll();
    
}
