/*
 * Licsense Header
 */
package Service;

import Domain.Product.BusinessRule;
import Persistence.BusinessRuleDAO;
import Persistence.GenericDAO;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Max
 */
public class BusinessRuleService {

    private EntityManagerFactory entityManagerFactory;
    private GenericDAO brDao;

    public BusinessRuleService() {
        try {
            entityManagerFactory = HibernateUtil.getEntityManagerFactory();
            brDao = new BusinessRuleDAO(entityManagerFactory);
            entityManagerFactory.close();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object");
            throw new ExceptionInInitializerError(ex);
        }
    }

    public BusinessRule getBusinessRule(String id) {
        System.out.println("Getting businessrule");
        entityManagerFactory = HibernateUtil.getEntityManagerFactory();
        brDao = new BusinessRuleDAO(entityManagerFactory);
        BusinessRule br = (BusinessRule) brDao.getByID(id);
        entityManagerFactory.close();
        return br;
    }
//testing
    public void saveCode(BusinessRule br, String s) {
        entityManagerFactory = HibernateUtil.getEntityManagerFactory();
        brDao = new BusinessRuleDAO(entityManagerFactory);
        System.out.println("Saving code: " + s);
        BusinessRule newInstance = br;
        br.setGencode(s);
        brDao.change(br, newInstance);
        entityManagerFactory.close();
    }
}
