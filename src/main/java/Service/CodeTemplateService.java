/*
 * Licsense Header
 */
package Service;

import Domain.ProductBeheer.CodeTemplate;
import Persistence.CodeTemplateDAO;
import Persistence.GenericDAO;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Max
 */
public class CodeTemplateService {

    private EntityManagerFactory entityManagerFactory;
    private GenericDAO ctd;

    public CodeTemplateService() {
        try {
            entityManagerFactory = HibernateUtil.getEntityManagerFactory();
            ctd = new CodeTemplateDAO(entityManagerFactory);
            entityManagerFactory.close();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object");
            throw new ExceptionInInitializerError(ex);
        }

    }

    public CodeTemplate getCodeTemplate(String id) {
        entityManagerFactory = HibernateUtil.getEntityManagerFactory();
        ctd = new CodeTemplateDAO(entityManagerFactory);
        CodeTemplate ct = (CodeTemplate) ctd.getByID(id);
        entityManagerFactory.close();
        return ct;
    }
}
