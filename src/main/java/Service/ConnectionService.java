package Service;

import Domain.ProductBeheer.ConnectionInfo;
import Persistence.ConnectionDAO;
import Persistence.GenericDAO;
import javax.persistence.EntityManagerFactory;

public class ConnectionService {

    private EntityManagerFactory entityManagerFactory;
    private GenericDAO connDAO;

    public ConnectionService() {
        try {
            entityManagerFactory = HibernateUtil.getEntityManagerFactory();
            connDAO = new ConnectionDAO(entityManagerFactory);
            entityManagerFactory.close();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object");
            throw new ExceptionInInitializerError(ex);
        }
    }

    public ConnectionInfo getConnection(String id) {
        System.out.println("Getting connection information");
        entityManagerFactory = HibernateUtil.getEntityManagerFactory();
        connDAO = new ConnectionDAO(entityManagerFactory);
        ConnectionInfo conn = (ConnectionInfo) connDAO.getByID(id);
        entityManagerFactory.close();
        return conn;
    }

    public void save(ConnectionInfo c) {
        System.out.println("Adding connection to the tool db");
        entityManagerFactory = HibernateUtil.getEntityManagerFactory();
        connDAO = new ConnectionDAO(entityManagerFactory);
        connDAO.create(c);
        entityManagerFactory.close();
    }
}
