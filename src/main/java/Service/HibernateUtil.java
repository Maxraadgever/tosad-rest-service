package Service;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateUtil {

    private static String orclcfg = "nl.hu.ict.jpa.oracle";
    private static String mysqlcfg = "nl.hu.ict.jpa.mysql";
    private static boolean mysql = false;
    private static String dbcfg = orclcfg;

    public static EntityManagerFactory getEntityManagerFactory() {
        return Persistence.createEntityManagerFactory(dbcfg);
    }

   
}
