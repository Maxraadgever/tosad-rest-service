/*
 * Licsense Header
 */
package restcontrollers;

import Domain.ProductBeheer.ConnectionInfo;
import com.opensymphony.xwork2.ModelDriven;

/**
 *
 * @author Max
 */
public class ConnectionRestController implements ModelDriven<Object> {

    private Object model = "no connection found";
    private String id, desc, uname, pass, host, port, service;

    public void view() {
        model = testConnection();
    }

    public Object getModel() {
        System.out.println("ConnectionRestController.getModel(): Get Model: " + model);
        return model;
    }

    public String testConnection() {
        String s = "Connection saved";
        boolean b = ConnectionInfo.testConnection(id);
        if (b) {
            ConnectionInfo.createView(id);
        } else {
            s = "Connection failed. The connection timed out\n"
                    + "There might be something wrong with the connection data\n"
                    + "Or the database is not online";

        }

        return s;
    }

    

    public String getUrl() {
        return "jdbc:oracle:thin:@" + host + ":" + port + ":" + service;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
