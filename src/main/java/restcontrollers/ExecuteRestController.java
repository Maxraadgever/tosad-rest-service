package restcontrollers;

import Domain.Product.BusinessRule;
import Domain.ProductBeheer.ConnectionInfo;
import com.opensymphony.xwork2.ModelDriven;

public class ExecuteRestController implements ModelDriven<Object> {

    private Object model;
    private String id;

    public void setId(String id) {
        this.id = id;
    }

    public void view() {
        BusinessRule br = BusinessRule.getBusinessRule(id);
        if (br.getCode() != null) {
            String connectionid = br.getConnection_id().getConnection_id() + "";
            boolean b = ConnectionInfo.execute(br, connectionid);
            if(b){
                model = "Code executed succesfully";
            }else{
                model = "Something went wrong while executing code\n"
                        + "See the glassfish console for more info";
            }
        } else {
            model = "This rule is not generated yet. Pleas generate it before executing";
        }

    }

    public Object getModel() {
        return model;
    }

}
