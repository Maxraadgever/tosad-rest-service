package restcontrollers;

import Domain.Product.BusinessRule;
import Domain.Product.Value;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;
import com.opensymphony.xwork2.ModelDriven;

public class GenerateRestController implements ModelDriven<Object> {

    private String id;
    private String language;

    private Object model = "ERROR";

    public HttpHeaders index() {
        System.out.println("RUNNING INDEX() METHOD");
        DefaultHttpHeaders dhh = new DefaultHttpHeaders("index");
        return new DefaultHttpHeaders("index").disableCaching();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLanguage(String lang) {
        language = lang;
    }

    public void view() {
        // This method runs the generate function on the business rule
        // and is run when the url = /rest/businessrule/#ID#
        // fill in the id of the businessrule that needs to be generated.
        BusinessRule br = BusinessRule.getBusinessRule(id);
        System.out.println("NAME: " + br.getDescription());
        for (Value v : br.getValues()) {
            System.out.println("VALUE" + v.getValue());
        }
        System.out.println("TYPE: " + br.getBusinessRuleType().getName());
        String s = "";
        try {
            if (br.getGencode() != null) {
                System.out.println("Found pregenerated code");
                s = br.getGencode();
            } else {
                System.out.println("Generating code");
                s = br.getGeneratedCode(language);
                BusinessRule.saveCode(br, s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        model = s;
        System.out.println(s);
        System.out.println("END VIEW METHOD");
    }

    public void create(String brCode) {
        System.out.println("create --" + id + "----");
    }

    public Object getModel() {
        System.out.println("GETMODEL ----------");

        return model;
    }

}
